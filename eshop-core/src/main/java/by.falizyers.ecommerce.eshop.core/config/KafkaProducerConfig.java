package by.falizyers.ecommerce.eshop.core.config;

import by.falizyers.ecommerce.eshop.core.dto.AuctionPricesDto;
import by.falizyers.ecommerce.eshop.core.dto.UsersDto;
import by.falizyers.ecommerce.eshop.core.entity.embedded.AuctionPricesId;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaProducerConfig {

    @Value("127.0.0.1:9092")
    private String kafkaServer;

    @Value("server.broadcast")
    private String kafkaProducerId;

    @Bean
    public Map<String, Object> producerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServer);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, kafkaProducerId);
        return props;
    }

    @Bean
    public ProducerFactory<AuctionPricesId, AuctionPricesDto> producerAuctionPricesFactory() {
        return new DefaultKafkaProducerFactory<>(producerConfigs());
    }

    @Bean
    public KafkaTemplate<AuctionPricesId, AuctionPricesDto> auctionPricesTemplate() {
        KafkaTemplate<AuctionPricesId, AuctionPricesDto> template = new KafkaTemplate<>(producerAuctionPricesFactory());
        template.setMessageConverter(new StringJsonMessageConverter());
        return template;
    }
}
