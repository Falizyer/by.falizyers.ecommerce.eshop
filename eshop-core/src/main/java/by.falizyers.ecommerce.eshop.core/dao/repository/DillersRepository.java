package by.falizyers.ecommerce.eshop.core.dao.repository;

import by.falizyers.ecommerce.eshop.core.entity.DillersEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface DillersRepository extends DataRepository<DillersEntity, Long> {
}
