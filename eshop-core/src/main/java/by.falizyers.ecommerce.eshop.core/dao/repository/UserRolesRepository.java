package by.falizyers.ecommerce.eshop.core.dao.repository;

import by.falizyers.ecommerce.eshop.core.entity.UserRolesEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRolesRepository extends DataRepository<UserRolesEntity, Long> {
}
