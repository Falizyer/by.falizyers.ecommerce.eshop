package by.falizyers.ecommerce.eshop.core.dao.repository;

import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends DataRepository<UsersEntity, Long> {
}
