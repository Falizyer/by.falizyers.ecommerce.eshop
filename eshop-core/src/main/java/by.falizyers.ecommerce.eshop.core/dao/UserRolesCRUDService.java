package by.falizyers.ecommerce.eshop.core.dao;

import by.falizyers.ecommerce.eshop.core.converter.UserRolesConverter;
import by.falizyers.ecommerce.eshop.core.converter.UsersConverter;
import by.falizyers.ecommerce.eshop.core.dao.repository.UserRolesRepository;
import by.falizyers.ecommerce.eshop.core.dao.repository.UsersRepository;
import by.falizyers.ecommerce.eshop.core.dto.UserRolesDto;
import by.falizyers.ecommerce.eshop.core.dto.UsersDto;
import by.falizyers.ecommerce.eshop.core.entity.UserRolesEntity;
import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
public class UserRolesCRUDService extends AbstractCRUDService<UserRolesEntity, UserRolesDto, Long> {

    @Autowired
    private UserRolesConverter userRolesConverter;

    @Autowired
    private UserRolesRepository userRolesRepository;

    @Override
    public UserRolesConverter getConverter() {
        return userRolesConverter;
    }

    @Override
    public UserRolesRepository getRepository() {
        return userRolesRepository;
    }
}
