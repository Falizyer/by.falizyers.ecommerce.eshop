package by.falizyers.ecommerce.eshop.core.dao;

import by.falizyers.ecommerce.eshop.core.converter.DillersConverter;
import by.falizyers.ecommerce.eshop.core.converter.UsersConverter;
import by.falizyers.ecommerce.eshop.core.dao.repository.DillersRepository;
import by.falizyers.ecommerce.eshop.core.dao.repository.UsersRepository;
import by.falizyers.ecommerce.eshop.core.dto.DillersDto;
import by.falizyers.ecommerce.eshop.core.dto.UsersDto;
import by.falizyers.ecommerce.eshop.core.entity.DillersEntity;
import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
public class DillersCRUDService extends AbstractCRUDService<DillersEntity, DillersDto, Long> {

    @Autowired
    private DillersConverter dillersConverter;

    @Autowired
    private DillersRepository dillersRepository;

    @Override
    public DillersConverter getConverter() {
        return dillersConverter;
    }

    @Override
    public DillersRepository getRepository() {
        return dillersRepository;
    }
}
