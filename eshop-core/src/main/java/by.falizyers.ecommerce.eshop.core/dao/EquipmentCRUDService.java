package by.falizyers.ecommerce.eshop.core.dao;

import by.falizyers.ecommerce.eshop.core.converter.EquipmentsConverter;
import by.falizyers.ecommerce.eshop.core.dao.repository.EquipmentRepository;
import by.falizyers.ecommerce.eshop.core.dto.EquipmentsDto;
import by.falizyers.ecommerce.eshop.core.entity.EquipmentsEntity;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
public class EquipmentCRUDService extends AbstractCRUDService<EquipmentsEntity, EquipmentsDto, Long> {

    @Autowired
    private EquipmentsConverter equipmentConverter;

    @Autowired
    private EquipmentRepository equipmentRepository;

    @Override
    public EquipmentsConverter getConverter() {
        return equipmentConverter;
    }

    @Override
    public EquipmentRepository getRepository() {
        return equipmentRepository;
    }
}
