package by.falizyers.ecommerce.eshop.core.dao;

import by.falizyers.ecommerce.eshop.core.converter.UsersConverter;
import by.falizyers.ecommerce.eshop.core.dao.repository.UserRolesRepository;
import by.falizyers.ecommerce.eshop.core.dao.repository.UsersRepository;
import by.falizyers.ecommerce.eshop.core.dto.UserRolesDto;
import by.falizyers.ecommerce.eshop.core.dto.UsersDto;
import by.falizyers.ecommerce.eshop.core.entity.UserRolesEntity;
import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@NoArgsConstructor
public class UsersCRUDService extends AbstractCRUDService<UsersEntity, UsersDto, Long> {

    @Autowired
    private UsersConverter usersConverter;

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UsersConverter getConverter() {
        return usersConverter;
    }

    @Override
    public UsersRepository getRepository() {
        return usersRepository;
    }
}
