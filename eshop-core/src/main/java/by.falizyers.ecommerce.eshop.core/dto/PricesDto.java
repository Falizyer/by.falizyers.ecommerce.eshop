package by.falizyers.ecommerce.eshop.core.dto;

import by.falizyers.ecommerce.eshop.core.dto.embedded.PricesDtoId;
import lombok.Data;

@Data
public class PricesDto implements PersistentEntityDto<PricesDtoId> {

    private PricesDtoId id;

    private Double price;
}
