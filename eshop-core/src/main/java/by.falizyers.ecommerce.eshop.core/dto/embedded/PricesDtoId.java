package by.falizyers.ecommerce.eshop.core.dto.embedded;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PricesDtoId {

    private Long dillerId;

    private Long equipmentId;
}
