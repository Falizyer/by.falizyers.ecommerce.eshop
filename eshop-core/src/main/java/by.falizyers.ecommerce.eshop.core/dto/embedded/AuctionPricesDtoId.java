package by.falizyers.ecommerce.eshop.core.dto.embedded;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuctionPricesDtoId {

    private Long lotId;

    private Long userId;
}
