package by.falizyers.ecommerce.eshop.core.dto;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
public @interface ReadOnlyDtoAttribute {
}
