package by.falizyers.ecommerce.eshop.core.dto;

import java.util.Date;

public interface TrackUpdateEntityDto {

    Long getCreationDate();

    Long getUpdateDate();

    void setCreationDate(Long date);

    void setUpdateDate(Long date);

}
