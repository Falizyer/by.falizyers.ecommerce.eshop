package by.falizyers.ecommerce.eshop.core.dto;

public enum TimezoneType {
    UTC_M_1,
    UTC_M_2,
    UTC_M_3,
    UTC_M_4,
    UTC_M_5,
    UTC_M_6,
    UTC_M_7,
    UTC_M_8,
    UTC_M_9,
    UTC_M_10,
    UTC_M_11,
    UTC_M_12,

    UTC_0,

    UTC_P_1,
    UTC_P_2,
    UTC_P_3,
    UTC_P_4,
    UTC_P_5,
    UTC_P_6,
    UTC_P_7,
    UTC_P_8,
    UTC_P_9,
    UTC_P_10,
    UTC_P_11,
    UTC_P_12
}
