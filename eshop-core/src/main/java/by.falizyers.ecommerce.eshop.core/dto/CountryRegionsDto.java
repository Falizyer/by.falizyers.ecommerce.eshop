package by.falizyers.ecommerce.eshop.core.dto;

import lombok.Data;

@Data
public class CountryRegionsDto implements PersistentEntityDto<String> {

    @ReadOnlyDtoAttribute
    private String id;

    private String codeShort;

    private String name;
}
