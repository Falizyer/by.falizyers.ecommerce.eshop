package by.falizyers.ecommerce.eshop.core.dto;

import lombok.Data;

import java.util.List;

@Data
public class CountriesDto implements PersistentEntityDto<String> {

    @ReadOnlyDtoAttribute
    private String id;

    private String codeShort;

    private String name;

    private List<CountryRegionsDto> regions;
}
