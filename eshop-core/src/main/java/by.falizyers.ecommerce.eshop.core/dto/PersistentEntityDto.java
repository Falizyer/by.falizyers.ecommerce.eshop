package by.falizyers.ecommerce.eshop.core.dto;

public interface PersistentEntityDto<ID> {

    ID getId();
}
