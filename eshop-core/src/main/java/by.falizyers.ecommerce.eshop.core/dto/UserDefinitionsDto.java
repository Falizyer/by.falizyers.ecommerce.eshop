package by.falizyers.ecommerce.eshop.core.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDefinitionsDto implements PersistentEntityDto<Long>, TrackUpdateEntityDto {

    @ReadOnlyDtoAttribute
    private Long id;

    private String firstName;

    private String lastName;

    private String middleName;

    private SupportedLanguages language;

    private TimezoneType timezone;

    private CountriesDto country;

    private CountryRegionsDto region;

    private Long creationDate;

    private Long updateDate;
}
