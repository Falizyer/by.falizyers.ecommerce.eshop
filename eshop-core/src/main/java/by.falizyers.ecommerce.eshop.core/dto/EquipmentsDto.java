package by.falizyers.ecommerce.eshop.core.dto;

import by.falizyers.ecommerce.eshop.core.entity.EquipmentsType;
import lombok.Data;

import java.util.List;

@Data
public class EquipmentsDto implements PersistentEntityDto<Long> {

    private Long id;

    private EquipmentsType type;

    private String name;

    private String code;

    private String description;

    private List<PricesDto> prices;

    private Double avgPrice;
}
