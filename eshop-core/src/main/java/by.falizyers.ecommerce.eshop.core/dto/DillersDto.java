package by.falizyers.ecommerce.eshop.core.dto;

import lombok.Data;

@Data
public class DillersDto implements PersistentEntityDto<Long> {

    private Long id;

    private String name;

    private String description;

    private Integer cellPhone;
}
