package by.falizyers.ecommerce.eshop.core.dto;

public enum SupportedLanguages {
    ENGLISH,
    RUSSIAN
}
