package by.falizyers.ecommerce.eshop.core.dto;

import by.falizyers.ecommerce.eshop.core.dto.embedded.AuctionPricesDtoId;
import lombok.Data;

@Data
public class AuctionPricesDto implements PersistentEntityDto<AuctionPricesDtoId> {

    private AuctionPricesDtoId id;

    private Double price;
}
