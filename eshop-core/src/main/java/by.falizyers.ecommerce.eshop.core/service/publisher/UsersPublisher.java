package by.falizyers.ecommerce.eshop.core.service.publisher;

import by.falizyers.ecommerce.eshop.core.converter.UsersConverter;
import by.falizyers.ecommerce.eshop.core.dto.UsersDto;
import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import io.reactivex.*;
import io.reactivex.functions.Predicate;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.subjects.PublishSubject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.security.auth.Subject;

@Component
public class UsersPublisher {

    private final Flowable<UsersEntity> publisher;

    private PublishSubject<UsersEntity> subject;

    @Autowired
    private KafkaTemplate<Long, UsersDto> usersDtoKafkaTemplate;

    @Autowired
    private UsersConverter usersConverter;

    public UsersPublisher() {
        this.subject = PublishSubject.create();

        ConnectableObservable<UsersEntity> connectableObservable = subject.share().publish();
        connectableObservable.connect();

        publisher = connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
    }

    public Flowable<UsersEntity> getPublisher(Long id) {
        return publisher.filter(isIdEquals(id));
    }

    @KafkaListener(id = "UsersDto", topics = {"server.users_dto"}, containerFactory = "singleFactory")
    public void consumeUser(UsersDto dto) {
        subject.onNext(usersConverter.convertFromDto(null, dto));
    }

    public void send(UsersDto dto) {
        usersDtoKafkaTemplate.send("server.users_dto", dto);
    }

    private Predicate<UsersEntity> isIdEquals(Long id) {
        return p -> p.getId().equals(id);
    }
}
