package by.falizyers.ecommerce.eshop.core.service.publisher;

import by.falizyers.ecommerce.eshop.core.converter.AuctionPricesConverter;
import by.falizyers.ecommerce.eshop.core.converter.UsersConverter;
import by.falizyers.ecommerce.eshop.core.dto.AuctionPricesDto;
import by.falizyers.ecommerce.eshop.core.dto.UsersDto;
import by.falizyers.ecommerce.eshop.core.dto.embedded.AuctionPricesDtoId;
import by.falizyers.ecommerce.eshop.core.entity.AuctionPricesEntity;
import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import by.falizyers.ecommerce.eshop.core.entity.embedded.AuctionPricesId;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.functions.Predicate;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.subjects.PublishSubject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class AuctionPricesPublisher {

    private final Flowable<AuctionPricesEntity> publisher;

    private PublishSubject<AuctionPricesEntity> subject;

    @Autowired
    private KafkaTemplate<AuctionPricesId, AuctionPricesDto> auctionPricesTemplate;

    @Autowired
    private AuctionPricesConverter auctionPricesConverter;

    public AuctionPricesPublisher() {
        this.subject = PublishSubject.create();

        ConnectableObservable<AuctionPricesEntity> connectableObservable = subject.share().publish();
        connectableObservable.connect();

        publisher = connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
    }

    public Flowable<AuctionPricesEntity> getPublisher(AuctionPricesId id) {
        return publisher.filter(p -> p.getId().equals(id));
    }

    @KafkaListener(groupId = "eshop", topics = {"server.auction_prices"}, containerFactory = "singleFactory")
    public void consumeUser(AuctionPricesDto dto) {
        subject.onNext(auctionPricesConverter.convertFromDto(null, dto));
    }

    public void send(AuctionPricesDto dto) {
        auctionPricesTemplate.send("server.auction_prices", dto);
    }
}
