package by.falizyers.ecommerce.eshop.core.entity;

import java.util.Date;

public interface TrackUpdateEntity {

    Date getCreationDate();

    Date getUpdateDate();

    void setCreationDate(Date date);

    void setUpdateDate(Date date);

}
