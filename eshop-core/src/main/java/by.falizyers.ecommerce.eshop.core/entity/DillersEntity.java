package by.falizyers.ecommerce.eshop.core.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "dillers")
public class DillersEntity implements PersistentEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dlrs_id")
    private Long id;

    @Column(name = "dlrs_name")
    private String name;

    @Column(name = "dlrs_description")
    private String description;

    @Column(name = "dlrs_cell_phone")
    private Integer cellPhone;
}
