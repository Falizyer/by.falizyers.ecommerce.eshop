package by.falizyers.ecommerce.eshop.core.entity;

import by.falizyers.ecommerce.eshop.core.entity.embedded.PricesEntityId;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "equipment_prices")
public class PricesEntity implements PersistentEntity<PricesEntityId> {

    @EmbeddedId
    private PricesEntityId id;

    @Column(name = "etps_price")
    private Double price;

    @ManyToOne
    @JoinColumn(name = "etps_dlrs_id", insertable = false, updatable = false)
    private DillersEntity diller;
}
