package by.falizyers.ecommerce.eshop.core.entity;

import by.falizyers.ecommerce.eshop.core.dto.SupportedLanguages;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "user_definitions")
public class UserDefinitionsEntity implements VersionableEntity<Long>, TrackUpdateEntity {

    @Id
    @Column(name = "udfn_id")
    private Long id;

    @Column(name = "udfn_first_name")
    private String firstName;

    @Column(name = "udfn_last_name")
    private String lastName;

    @Column(name = "udfn_middle_name")
    private String middleName;

    @Column(name = "udfn_language")
    @Enumerated
    private SupportedLanguages language;

    @Column(name = "udfn_timezone")
    private Integer timezone;

    @Column(name = "udfn_creation_date")
    private Date creationDate;

    @Column(name = "udfn_update_date")
    private Date updateDate;

    @Version
    @Column(name = "udfn_version")
    private Integer version;

    @ManyToOne
    @JoinColumn(name = "udfn_cnrs_code", insertable = false, updatable = false)
    private CountriesEntity country;

    @ManyToOne
    @JoinColumn(name = "udfn_crns_code", insertable = false, updatable = false)
    private CountryRegionsEntity region;

}
