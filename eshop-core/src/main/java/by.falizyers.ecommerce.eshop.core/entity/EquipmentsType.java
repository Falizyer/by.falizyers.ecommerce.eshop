package by.falizyers.ecommerce.eshop.core.entity;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public enum EquipmentsType {
    MOTHER_BOARD,
    GRAPHIC_CARD
}
