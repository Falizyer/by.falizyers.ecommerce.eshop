package by.falizyers.ecommerce.eshop.core.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "countries")
public class CountriesEntity implements PersistentEntity<String> {

    @Id
    @Column(name = "cnrs_code")
    private String id;

    @Column(name = "cnrs_code_short")
    private String codeShort;

    @Column(name = "cnrs_name")
    private String name;

    @OneToMany
    @JoinColumn(name = "cnrs_crns_code", insertable = false, updatable = false)
    private List<CountryRegionsEntity> regions;
}
