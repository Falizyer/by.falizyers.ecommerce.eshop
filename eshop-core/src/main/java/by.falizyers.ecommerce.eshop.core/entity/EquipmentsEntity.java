package by.falizyers.ecommerce.eshop.core.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Iterator;
import java.util.List;

@Data
@Entity
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@DiscriminatorColumn(name = "eqts_type", discriminatorType = DiscriminatorType.STRING)
@Table(name = "equipments")
public class EquipmentsEntity implements PersistentEntity<Long> {

    @Id
    @SequenceGenerator(name = "equipments_generator", sequenceName = "equipments_eqts_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "equipments_generator")
    @Column(name = "eqts_id")
    private Long id;

    @Enumerated
    @Column(name = "eqts_type")
    private EquipmentsType type;

    @Column(name = "eqts_name")
    private String name;

    @Column(name = "eqts_code")
    private String code;

    @Column(name = "eqts_description")
    private String description;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "equipment_prices",
            joinColumns = @JoinColumn(name = "etps_eqts_id", insertable = false, updatable = false),
            inverseJoinColumns = {
                    @JoinColumn(name = "etps_eqts_id", insertable = false, updatable = false),
                    @JoinColumn(name = "etps_dlrs_id", insertable = false, updatable = false)
            })
    private List<PricesEntity> prices;

    public Double getAvgPrice() {
        if (this.prices == null || this.prices.size() == 0) {
            return 0.0;
        }
        Iterator<PricesEntity> iter = this.prices.iterator();
        Double total = 0.0;
        while (iter.hasNext()) {
            total += iter.next().getPrice();
        }
        return total / this.prices.size();
    }
}
