package by.falizyers.ecommerce.eshop.core.entity.embedded;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PricesEntityId implements CompositeId {

    @Column(name = "etps_dlrs_id")
    private Long dillerId;

    @Column(name = "etps_eqts_id")
    private Long equipmentId;
}
