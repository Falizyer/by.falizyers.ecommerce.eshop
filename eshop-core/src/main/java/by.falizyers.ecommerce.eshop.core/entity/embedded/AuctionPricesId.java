package by.falizyers.ecommerce.eshop.core.entity.embedded;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuctionPricesId implements CompositeId {

    @Column(name = "acps_lot_id")
    private Long lotId;

    @Column(name = "acps_usrs_id")
    private Long userId;
}
