package by.falizyers.ecommerce.eshop.core.entity;

public interface VersionableEntity<ID> extends PersistentEntity<ID> {
}
