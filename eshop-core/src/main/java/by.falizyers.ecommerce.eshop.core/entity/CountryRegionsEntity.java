package by.falizyers.ecommerce.eshop.core.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "country_regions")
public class CountryRegionsEntity implements PersistentEntity<String> {

    @Id
    @GeneratedValue
    @Column(name = "crns_code")
    private String id;

    @Column(name = "crns_code_short")
    private String codeShort;

    @Column(name = "crns_name")
    private String name;
}
