package by.falizyers.ecommerce.eshop.core.entity;

import by.falizyers.ecommerce.eshop.core.entity.embedded.AuctionPricesId;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "auction_prices")
public class AuctionPricesEntity implements PersistentEntity<AuctionPricesId> {

    @EmbeddedId
    private AuctionPricesId id;

    @Column(name = "acps_price")
    private Double price;
}
