package by.falizyers.ecommerce.eshop.core.entity;

public interface PersistentEntity<ID> {

    ID getId();
}
