package by.falizyers.ecommerce.eshop.core.converter;

import by.falizyers.ecommerce.eshop.core.dto.AuctionPricesDto;
import by.falizyers.ecommerce.eshop.core.dto.embedded.AuctionPricesDtoId;
import by.falizyers.ecommerce.eshop.core.entity.AuctionPricesEntity;
import by.falizyers.ecommerce.eshop.core.entity.embedded.AuctionPricesId;
import org.springframework.stereotype.Service;

@Service
public class AuctionPricesConverter extends AbstractConverter<AuctionPricesEntity, AuctionPricesDto> {

    @Override
    public AuctionPricesDto newDto() {
        return new AuctionPricesDto();
    }

    @Override
    public AuctionPricesEntity newEntity() {
        return new AuctionPricesEntity();
    }

    @Override
    public AuctionPricesDto convertToDto(AuctionPricesEntity entity, AuctionPricesDto dto) {
        if (dto == null) {
            dto = newDto();
        }


        dto.setId(new AuctionPricesDtoId(entity.getId().getLotId(), entity.getId().getUserId()));
        dto.setPrice(entity.getPrice());

        return dto;
    }

    @Override
    public AuctionPricesEntity convertFromDto(AuctionPricesEntity entity, AuctionPricesDto dto) {
        if (entity == null) {
            entity = newEntity();
        }

        entity.setId(new AuctionPricesId(dto.getId().getLotId(), dto.getId().getUserId()));
        entity.setPrice(dto.getPrice());

        return entity;
    }
}
