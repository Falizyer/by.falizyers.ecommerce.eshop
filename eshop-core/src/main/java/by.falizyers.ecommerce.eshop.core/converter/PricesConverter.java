package by.falizyers.ecommerce.eshop.core.converter;

import by.falizyers.ecommerce.eshop.core.dto.PricesDto;
import by.falizyers.ecommerce.eshop.core.dto.embedded.PricesDtoId;
import by.falizyers.ecommerce.eshop.core.entity.PricesEntity;
import by.falizyers.ecommerce.eshop.core.entity.embedded.PricesEntityId;
import org.springframework.stereotype.Service;

@Service
public class PricesConverter extends AbstractConverter<PricesEntity, PricesDto> {

    @Override
    public PricesDto newDto() {
        return new PricesDto();
    }

    @Override
    public PricesEntity newEntity() {
        return new PricesEntity();
    }

    @Override
    public PricesDto convertToDto(PricesEntity entity, PricesDto dto) {
        if (dto == null) {
            dto = newDto();
        }

        dto.setId(new PricesDtoId(entity.getId().getDillerId(), entity.getId().getEquipmentId()));
        dto.setPrice(entity.getPrice());

        return dto;
    }

    @Override
    public PricesEntity convertFromDto(PricesEntity entity, PricesDto dto) {
        if (entity == null) {
            entity = newEntity();
        }

        entity.setId(new PricesEntityId(dto.getId().getDillerId(), dto.getId().getEquipmentId()));
        entity.setPrice(dto.getPrice());

        return entity;
    }
}
