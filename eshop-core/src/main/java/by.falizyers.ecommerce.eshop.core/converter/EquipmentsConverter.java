package by.falizyers.ecommerce.eshop.core.converter;

import by.falizyers.ecommerce.eshop.core.dto.EquipmentsDto;
import by.falizyers.ecommerce.eshop.core.entity.EquipmentsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EquipmentsConverter extends AbstractConverter<EquipmentsEntity, EquipmentsDto> {

    @Autowired
    private PricesConverter pricesConverter;

    @Override
    public EquipmentsDto newDto() {
        return new EquipmentsDto();
    }

    @Override
    public EquipmentsEntity newEntity() {
        return new EquipmentsEntity();
    }

    @Override
    public EquipmentsDto convertToDto(EquipmentsEntity entity, EquipmentsDto dto) {
        if (dto == null) {
            dto = newDto();
        }

        dto.setId(entity.getId());

        return dto;
    }

    @Override
    public EquipmentsEntity convertFromDto(EquipmentsEntity entity, EquipmentsDto dto) {
        if (entity == null) {
            entity = newEntity();
        }

        entity.setId(dto.getId());
        entity.setCode(dto.getCode());
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setType(dto.getType());

        if (dto.getPrices() != null) {
            entity.setPrices(pricesConverter.convertFromDtoList(dto.getPrices()));
        }

        return entity;
    }
}
