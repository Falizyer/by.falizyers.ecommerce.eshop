package by.falizyers.ecommerce.eshop.core.converter;

import by.falizyers.ecommerce.eshop.core.dto.DillersDto;
import by.falizyers.ecommerce.eshop.core.entity.DillersEntity;
import org.springframework.stereotype.Service;

@Service
public class DillersConverter extends AbstractConverter<DillersEntity, DillersDto> {

    @Override
    public DillersDto newDto() {
        return new DillersDto();
    }

    @Override
    public DillersEntity newEntity() {
        return new DillersEntity();
    }

    @Override
    public DillersDto convertToDto(DillersEntity entity, DillersDto dto) {
        if (dto == null) {
            dto = newDto();
        }

        dto.setId(entity.getId());

        return dto;
    }

    @Override
    public DillersEntity convertFromDto(DillersEntity entity, DillersDto dto) {
        if (entity == null) {
            entity = newEntity();
        }

        entity.setId(dto.getId());

        return entity;
    }
}
