package by.falizyers.ecommerce.eshop.core.converter;

import by.falizyers.ecommerce.eshop.core.dto.UserDefinitionsDto;
import by.falizyers.ecommerce.eshop.core.dto.UserRolesDto;
import by.falizyers.ecommerce.eshop.core.entity.UserDefinitionsEntity;
import by.falizyers.ecommerce.eshop.core.entity.UserRolesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDefinitionssConverter extends AbstractConverter<UserDefinitionsEntity, UserDefinitionsDto> {

    @Autowired
    private UserPermissionsConverter userPermissionsConverter;

    @Override
    public UserDefinitionsDto newDto() {
        return new UserDefinitionsDto();
    }

    @Override
    public UserDefinitionsEntity newEntity() {
        return new UserDefinitionsEntity();
    }

    @Override
    public UserDefinitionsDto convertToDto(UserDefinitionsEntity entity, UserDefinitionsDto dto) {
        if (dto == null) {
            dto = newDto();
        }

        dto.setId(entity.getId());
        dto.setCreationDate(entity.getCreationDate().getTime());
        dto.setUpdateDate(entity.getUpdateDate().getTime());

        return dto;
    }

    @Override
    public UserDefinitionsEntity convertFromDto(UserDefinitionsEntity entity, UserDefinitionsDto dto) {
        if (entity == null) {
            entity = newEntity();
        }
        return entity;
    }
}
