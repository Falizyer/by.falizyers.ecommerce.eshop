create table cart_items
(
    ctis_id      bigserial not null primary key,
    ctis_usrs_id bigserial not null,
    ctis_eqts_id bigint    not null,
    ctis_amount  int       not null,

    constraint fk_ctis_usrs_id foreign key (ctis_usrs_id) references users (usrs_id) on delete restrict,
    constraint fk_ctis_eqts_id foreign key (ctis_eqts_id) references equipments (eqts_id) on delete restrict
);

create table auction_calls
(
    ancs_id        bigserial not null primary key,
    ancs_eqts_id   bigint    not null,
    ancs_amount    int       not null,
    ancs_start_bid decimal   not null,
    ancs_bid       decimal   not null default ancs_start_bid,

    constraint fk_ancs_eqts_id foreign key (ancs_eqts_id) references equipments (eqts_id) on delete restrict
);

create table auction_call_prices
(
    acps_lot_id  bigint  not null,
    acps_usrs_id bigint  not null,
    acps_price   decimal not null,

    constraint fk_acps_lot_id foreign key (acps_lot_id) references auction_calls (ancs_id) on delete restrict,
    constraint fk_acps_usrs_id foreign key (acps_usrs_id) references users (usrs_id) on delete restrict
);

insert into cart_items
values (1, 1, 1, 10);

insert into auction_calls
values (1, 1, 100, 300.95);

insert into auction_call_prices
values (1, 1, 500.12);