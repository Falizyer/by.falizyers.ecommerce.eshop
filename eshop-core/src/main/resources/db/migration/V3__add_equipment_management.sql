create table dillers
(
    dlrs_id          bigserial    not null primary key,
    dlrs_name        varchar(255) not null unique,
    dlrs_description varchar(255) not null,
    dlrs_cell_phone  int          not null unique
);

create table equipments
(
    eqts_id          bigserial     not null primary key,
    eqts_type        int           not null,

    eqts_name        varchar(255)  not null,
    eqts_code        varchar(255)  not null unique,
    eqts_description varchar(2000) not null,

    eqts_height      int,
    eqts_width       int,
    eqts_weight      int
);

create table equipment_prices
(
    etps_dlrs_id bigint  not null,
    etps_eqts_id bigint  not null,

    etps_price   decimal not null,

    primary key (etps_dlrs_id, etps_eqts_id),
    constraint fk_etps_dlrs_id foreign key (etps_dlrs_id) references dillers (dlrs_id) on delete restrict,
    constraint fk_etps_eqts_id foreign key (etps_eqts_id) references equipments (eqts_id) on delete restrict
);

insert into dillers
values (1, 'Diller', 'Description', 0);

insert into equipments
values (1, 0, 'Equipment 1', 'CODE001',
        'Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem ' ||
        'aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. ' ||
        'Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni ' ||
        'dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, ' ||
        'amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam ' ||
        'aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, ' ||
        'nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, ' ||
        'quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? At vero eos et ' ||
        'accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos ' ||
        'dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia ' ||
        'deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. ' ||
        'Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, ' ||
        'facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis ' ||
        'debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque ' ||
        'earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis ' ||
        'doloribus asperiores repellat.',
        1.1, 1.2, 1.3),
       (2, 1, 'Equipment 2', 'CODE002',
        'Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem ' ||
        'aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. ' ||
        'Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni ' ||
        'dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, ' ||
        'amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam ' ||
        'aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, ' ||
        'nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, ' ||
        'quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? At vero eos et ' ||
        'accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos ' ||
        'dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia ' ||
        'deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. ' ||
        'Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, ' ||
        'facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis ' ||
        'debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque ' ||
        'earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis ' ||
        'doloribus asperiores repellat.',
        1.1, 1.2, 1.3);

insert into equipment_prices
values (1, 1, 255.10),
       (1, 2, 855.10);