package by.falizyers.ecommerce.eshop.api.resolver;

import by.falizyers.ecommerce.eshop.core.dao.EquipmentCRUDService;
import by.falizyers.ecommerce.eshop.core.entity.EquipmentsEntity;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EquipmentQuery implements GraphQLQueryResolver {

    @Autowired
    private EquipmentCRUDService equipmentsCRUDService;

    public List<EquipmentsEntity> getEquipments(int count, int offset) {
        return count == -1
                ? equipmentsCRUDService.getResourceList()
                : equipmentsCRUDService.getResourceList().subList(offset, count + offset);
    }

    public EquipmentsEntity getEquipment(Long equipmentId) {
        return equipmentsCRUDService.getResource(equipmentId);
    }

}
