package by.falizyers.ecommerce.eshop.api.resolver;

import by.falizyers.ecommerce.eshop.core.dto.UserPermissionsDto;
import by.falizyers.ecommerce.eshop.core.dto.UserRolesDto;
import by.falizyers.ecommerce.eshop.core.dto.UsersDto;
import by.falizyers.ecommerce.eshop.core.entity.UserPermissionsEntity;
import by.falizyers.ecommerce.eshop.core.entity.UserRolesEntity;
import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.stereotype.Component;

@Component
public class Mutation implements GraphQLMutationResolver {

    public UserRolesEntity createUserRole(UserRolesDto dto) {
        return new UserRolesEntity();
    }

    public UserRolesEntity updateUserRole(Long userRoleId, UserRolesDto dto) {
        return new UserRolesEntity();
    }

    public UserRolesEntity deleteUserRole(Long userRoleId) {
        return new UserRolesEntity();
    }

    public UserPermissionsEntity createUserPermission(UserPermissionsDto dto) {
        return new UserPermissionsEntity();
    }

    public UserPermissionsEntity updateUserPermission(Long userPermId, UserPermissionsDto dto) {
        return new UserPermissionsEntity();
    }

    public UserPermissionsEntity deleteUserPermission(Long userPermId) {
        return new UserPermissionsEntity();
    }
}
