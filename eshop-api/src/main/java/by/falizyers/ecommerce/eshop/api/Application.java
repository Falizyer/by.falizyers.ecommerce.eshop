package by.falizyers.ecommerce.eshop.api;

import by.falizyers.ecommerce.eshop.core.EnableCoreConfig;
import by.falizyers.ecommerce.eshop.core.config.KafkaConsumerConfig;
import by.falizyers.ecommerce.eshop.core.config.KafkaProducerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication(scanBasePackages = {
        "by.falizyers.ecommerce.eshop.core",
        "by.falizyers.ecommerce.eshop.api"
})
@EnableCoreConfig
@Import({KafkaConsumerConfig.class, KafkaProducerConfig.class})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
