package by.falizyers.ecommerce.eshop.api.controller;

import by.falizyers.ecommerce.eshop.core.converter.UsersConverter;
import by.falizyers.ecommerce.eshop.core.dao.UsersCRUDService;
import by.falizyers.ecommerce.eshop.core.dto.UsersDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {

    @Autowired
    private UsersCRUDService usersCRUDService;

    @Autowired
    private UsersConverter usersConverter;

    @GetMapping(value = "/users/{userId}")
    public @ResponseBody
    ResponseEntity<UsersDto> getUser(@PathVariable Long userId) {
        UsersDto dto = usersConverter.convertToDto(usersCRUDService.getResource(userId), null);
        return new ResponseEntity<UsersDto>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/users/{userId}")
    public @ResponseBody
    ResponseEntity<UsersDto> updateUser(@PathVariable Long userId,
                                        @RequestBody UsersDto dto) {
        return new ResponseEntity<UsersDto>(
                usersConverter.convertToDto(usersCRUDService.updateResource(userId, dto), null),
                HttpStatus.OK);
    }
}
