package by.falizyers.ecommerce.eshop.api.resolver;

import by.falizyers.ecommerce.eshop.core.dao.EquipmentCRUDService;
import by.falizyers.ecommerce.eshop.core.dao.UsersCRUDService;
import by.falizyers.ecommerce.eshop.core.dto.EquipmentsDto;
import by.falizyers.ecommerce.eshop.core.dto.UsersDto;
import by.falizyers.ecommerce.eshop.core.entity.EquipmentsEntity;
import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EquipmentMutation implements GraphQLMutationResolver {

    @Autowired
    private EquipmentCRUDService equipmentCRUDService;

    public EquipmentsEntity createEquipment(EquipmentsDto dto) {
        return equipmentCRUDService.saveResource(dto);
    }

    public EquipmentsEntity updateEquipment(Long userId, EquipmentsDto dto) {
        return equipmentCRUDService.updateResource(userId, dto);
    }

    public EquipmentsEntity deleteEquipment(Long userId) {
        return equipmentCRUDService.deleteResource(userId);
    }
}
