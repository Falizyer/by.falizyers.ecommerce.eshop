package by.falizyers.ecommerce.eshop.api.resolver;

import by.falizyers.ecommerce.eshop.core.dao.DillersCRUDService;
import by.falizyers.ecommerce.eshop.core.dao.UsersCRUDService;
import by.falizyers.ecommerce.eshop.core.entity.DillersEntity;
import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DillerQuery implements GraphQLQueryResolver {

    @Autowired
    private DillersCRUDService dillersCRUDService;

    public List<DillersEntity> getDillers(int count, int offset) {
        return count == -1
                ? dillersCRUDService.getResourceList()
                : dillersCRUDService.getResourceList().subList(offset, offset + count);
    }

    public DillersEntity getDiller(Long userId) {
        return dillersCRUDService.getResource(userId);
    }
}
