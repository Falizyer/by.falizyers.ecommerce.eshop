package by.falizyers.ecommerce.eshop.api.resolver;

import by.falizyers.ecommerce.eshop.core.converter.UsersConverter;
import by.falizyers.ecommerce.eshop.core.dao.UsersCRUDService;
import by.falizyers.ecommerce.eshop.core.dao.repository.UsersRepository;
import by.falizyers.ecommerce.eshop.core.dto.UsersDto;
import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMutation implements GraphQLMutationResolver {

    @Autowired
    private UsersCRUDService usersCRUDService;

    public UsersEntity createUser(UsersDto dto) {
        return usersCRUDService.saveResource(dto);
    }

    public UsersEntity updateUser(Long userId, UsersDto dto) {
        return usersCRUDService.updateResource(userId, dto);
    }

    public UsersEntity deleteUser(Long userId) {
        return usersCRUDService.deleteResource(userId);
    }
}
