package by.falizyers.ecommerce.eshop.api.resolver;

import by.falizyers.ecommerce.eshop.core.entity.UsersEntity;
import by.falizyers.ecommerce.eshop.core.service.publisher.UsersPublisher;
import com.coxautodev.graphql.tools.GraphQLSubscriptionResolver;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Subscription implements GraphQLSubscriptionResolver {

    @Autowired
    private UsersPublisher usersPublisher;

    public Publisher<UsersEntity> userUpdate(Long userId) {
        return usersPublisher.getPublisher(userId);
    }
}
